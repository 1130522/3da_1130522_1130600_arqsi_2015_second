/**

 *             Unidade curricular de Arquitetura de Sistemas - 1� Itera��o do trabalho pr�tico
 *             TRABALHO REALIZADO POR: Ricardo Leite (1130522) e Tiago Filipe (1130600) - Turma 3DA
 *             
 */


/**
 * 
 * Vari�veis url para efetuar pedidos Ajax;
 */
var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?";
var urlFacetas = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php";
var urlValoresFacetas = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=";
var urlTiposFacetas = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=";
var urlValorMinimoFaceta = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=";
var urlValorMaximoFaceta = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=";

/**
 * 
 * Vetores de controlo de pedidos ajax;
 */
var ajaxFacetas = {};
var ajaxTypes = {};
var ajaxMinMax = {};
var ajaxSearch = {};

/**
 * 
 * Vari�vel de desenho do widget(horizontal/vertical) 
 */
var displayType = "";

/**
 * Fun��o que permite iniciar o widget na p�gina hospedeira
 * @param {type} divWidget div para input do widget
 * @param {type} divOutput div para output do widget
 * @param {type} display tipo de display do widget (vertical ou horizontal)
 * @returns {undefined}
 */
function initWidget(divWidget, divOutput, display) {
    displayType = display;

    var divHead = document.createElement("OUTPUT");
    divHead.id = "divHead";
    divHead.value = "Widget ImoRest";

    var divFacetas = document.createElement("div");
    var divButtons = document.createElement("div");
    var space = document.createElement("br");
    divFacetas.id = "divFacetas";

    var button = document.createElement("BUTTON");
    button.id = "button";
    var text = document.createTextNode("Pesquisar");
    button.appendChild(text);
    button.addEventListener("click", function() {
        search(divOutput);
    });

    divButtons.appendChild(button);

    var widget = document.getElementById(divWidget);

    if (widget === null) {
        widget = document.getElementsByClassName(divWidget);
        while (widget[0].firstChild) {
            widget[0].removeChild(widget[0].firstChild);
        }

        widget[0].appendChild(divHead);
        widget[0].appendChild(divFacetas);
        widget[0].appendChild(space);
        widget[0].appendChild(divButtons);
    } else {
        while (widget.firstChild) {
            widget.removeChild(widget.firstChild);
        }

        widget.appendChild(divHead);
        widget.appendChild(divFacetas);
        widget.appendChild(space);
        widget.appendChild(divButtons);
    }

    getFacetas();
}

/**
 * 
 * Fun��o para pedido das facetas
 */
function getFacetas() {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxFacetas[0] = new XMLHttpRequest();
    } else {
        ajaxFacetas[0] = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (ajaxFacetas[0]) {
        ajaxFacetas[0].open("GET", urlFacetas, true);
        ajaxFacetas[0].onreadystatechange = getFacetasXML;
        ajaxFacetas[0].send(null);
    }
}

/**
 * 
 * Fun��o que extra� o nome das facetas do XML obtido
 */
function getFacetasXML() {
    if (ajaxFacetas[0].readyState == 4 && ajaxFacetas[0].status == 200) {
        var docXML = ajaxFacetas[0].responseXML;
        var facetas = [];
        facetas = docXML.getElementsByTagName("faceta");

        estruturaHTML(facetas);
    }
}

/**
 * Fun��o que estrutura o html e define os seus divs
 * @param {type} facetas vetor de facetas
 * @returns {undefined}
 */
function estruturaHTML(facetas) {
    var divFacetas = document.getElementById('divFacetas');
    var i = 0;
    for (i = 0; i < facetas.length; i++) {
        var divFaceta = document.createElement("div");
        divFaceta.id = i + 1;
        if (displayType.localeCompare("horizontal") == 0) {
            divFaceta.className = "facetaStyle";
        }

        var divValores = document.createElement("div");
        divValores.id = "valores_" + (i + 1);
        divValores.style.display = 'none';

        var span = document.createElement("span");
        var name = document.createTextNode(facetas[i].childNodes[0].nodeValue);

        span.appendChild(name);

        divFaceta.appendChild(span);
        var space = document.createElement("br");
        divFaceta.appendChild(space);
        divFaceta.appendChild(divValores);

        divFacetas.appendChild(divFaceta);

        if (displayType.localeCompare("vertical") == 0) {
            divFacetas.appendChild(space);
        }
    }

    preencherFacetas(facetas);
}

/**
 * Fun��o que preenche os checkboxs com a respetiva faceta
 * @param {type} facetas vetor de facetas
 * 
 */
function preencherFacetas(facetas) {
    var i = 0;
    for (i = 0; i < facetas.length; i++) {
        var divFaceta = document.getElementById(i + 1);

        var checkbox = document.createElement("INPUT");
        checkbox.type = "checkbox";
        checkbox.name = (i + 1);
        checkbox.addEventListener("click", function() {
            display(this, this.name);
        });

        divFaceta.insertBefore(checkbox, divFaceta.children[0]);

        getTiposFacetaAJAX(facetas[i].childNodes[0].nodeValue, i + 1);
    }
}

/**
 * Fun��o que especifica o display de uma checkbox
 * @param {type} checkbox 
 * @param {type} id da faceta
 * @returns {undefined}
 */
function display(checkbox, id) {
    var div = document.getElementById("valores_" + id);
    if (checkbox.checked == true) {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
}

/**
 * Fun��o que efetua o pedido ajax de uma faceta
 * @param {type} tipoFaceta tipo da faceta
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getValoresFacetaAJAX(tipoFaceta, name, id) {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxFacetas[id] = new XMLHttpRequest();
    } else {
        ajaxFacetas[id] = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (ajaxFacetas[id]) {
        ajaxFacetas[id].onreadystatechange = function() {
            getValoresFacetaJSON(tipoFaceta, name, id);
        };
        ajaxFacetas[id].open("GET", encodeURI(urlValoresFacetas + name), true);
        ajaxFacetas[id].send(null);
    }
}

/**
 * Fun��o que constroi um objeto JSON com os valores das facetas do pedido Ajax
 * @param {type} tipoFaceta tipo de faceta
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 * @returns {undefined}
 */
function getValoresFacetaJSON(tipoFaceta, name, id) {
    if (ajaxFacetas[id].readyState == 4 && ajaxFacetas[id].status == 200) {
        var text = ajaxFacetas[id].responseText;
        var data = [];
        data = JSON.parse(text);

        preencherFacetaHTML(tipoFaceta, data, name, id);
    }
}

/**
 * Fun��o que efetua o pedido do tipo da faceta
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 * @returns {undefined}
 */
function getTiposFacetaAJAX(name, id) {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxTypes[id] = new XMLHttpRequest();
    } else {
        ajaxTypes[id] = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (ajaxTypes[id]) {
        ajaxTypes[id].onreadystatechange = function() {
            getTiposFacetaJSON(name, id);
        };

        ajaxTypes[id].open("GET", encodeURI(urlTiposFacetas + name), true);
        ajaxTypes[id].send(null);
    }
}

/**
 * Fun��o que constroi o objeto JSON do tipo de faceta
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getTiposFacetaJSON(name, id) {
    if (ajaxTypes[id].readyState == 4 && ajaxTypes[id].status == 200) {
        var text = ajaxTypes[id].responseText;
        var data = JSON.parse(text);
        var data1 = JSON.stringify(text);

        var discreto = data.discreto;
        var semantica = data.semântica;
        var tipo = data.tipo;

        if (semantica.localeCompare("figura") != 0 && tipo.localeCompare("url") != 0) {
            getValoresFacetaAJAX(data, name, id);
        }
    }
}

/**
 * Fun��o que permite preecher os valores das facetas
 * @param {type} tipoFaceta tipo de faceta
 * @param {type} valoresFaceta valores da faceta
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function preencherFacetaHTML(tipoFaceta, valoresFaceta, name, id) {
    var i;
    var divValores = document.getElementById("valores_" + id);

    var discreto = tipoFaceta.discreto;
    var n = discreto.localeCompare("discreto");

    var semantica = tipoFaceta.semântica;
    var s = semantica.localeCompare("monetário");

    if (discreto.localeCompare("discreto") == 0) {
        valoresDiscretos(valoresFaceta, divValores);
    }

    if (discreto.localeCompare("discreto") == -1) {
        valoresContinuos(name, id, valoresFaceta, divValores, s);
    }
}

/**
 * Fun��o especifica para preenchimento de valores discretos no widget
 * @param {type} data vetor de valores da faceta
 * @param {type} div div onde colocar os valores
 * @returns {undefined}
 */
function valoresDiscretos(data, div) {
    var i = 0;
    for (i = 0; i < data.length; i++) {
        var checkbox = document.createElement("INPUT");
        checkbox.type = "checkbox";
        checkbox.name = data[i];
        var text = document.createTextNode(data[i]);

        var space = document.createElement("br");
        div.appendChild(checkbox);
        div.appendChild(text);
        div.appendChild(space);
    }
}

/**
 * Fun��o especifica para preenchimento de valores discretos no widget
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 * @param {type} data vetor de valores da faceta
 * @param {type} div div onde colocar os valores
 * @param {type} s semantica da faceta (0 se monet�rio / -1 se outro)
 * @returns {undefined}
 */
function valoresContinuos(name, id, data, div, s) {
    var pos = 0;
    var faceta = document.getElementById(id);
    var min = document.createElement("INPUT");
    var max = document.createElement("INPUT");
    min.type = "textbox";
    min.size = 6;

    max.type = "textbox";
    max.size = 6;

    var space = document.createElement("br");

    getValorMinimoFacetaAJAX(min, name, id); //vai efetuar aqui o pedido de valor minimo
    getValorMaxFacetaAJAX(max, name, id + 2);  //vai efetuar aqui o pedido de valor maximo

    if (s == 0) { //verificar semântica
        semanticaMonetaria(div, min, max);
    }

    if (s == -1) { //verificar semântica
        semanticaEspaco(div, min, max);
    }

    div.appendChild(space);
}

/**
 * Fun��o para desenho especifico de valores monet�rios
 * @param {type} div div onde colocar os valores
 * @param {type} min valor m�nimo
 * @param {type} max valor m�ximo
 */
function semanticaMonetaria(div, min, max) {
    var pattern = /[1-9]|[1-9][0-9]+/;
    var eur = document.createElement("OUTPUT");
    eur.value = "€";
    var eur1 = document.createElement("OUTPUT");
    eur1.value = "€";

    var minTxt = document.createElement("OUTPUT");
    minTxt.value = "Mínimo: ";

    var maxTxt = document.createElement("OUTPUT");
    maxTxt.value = "   Máximo: ";

    var space = document.createElement("br");

    min.addEventListener("change", function() {
        if (!pattern.test(min.value)) {
            min.value = "";
        } else if (min.value === "") {
            min.value === "";
        } else if (+min.value < min.name) {
            min.value = min.name;
        } else if ((max.value !== "") && (+min.value > +min.value)) {
            min.value = +max.value - 1;
        } else {
            min.value = this.value;

            if ((max.value !== "") && +max.value < +min.value) {
                var aux = max.value;

                max.value = min.value;
                min.value = aux;
            }
        }
    }
    );

    div.appendChild(minTxt);
    div.appendChild(min);
    div.appendChild(eur1);

    max.addEventListener("change", function() {
        if (!pattern.test(max.value)) {
            max.value = "";
        } else if (max.value === "") {
            max.value === "";
        } else if (+max.value > max.name) {
            max.value = max.name;
        } else if ((min.value !== "") && +max.value < +min.value) {
            max.value = +min.value + 1;
        } else {
            max.value = this.value;

            if ((min.value !== "") && +min.value > +max.value) {
                var aux = max.value;
                max.value = min.value;
                min.value = aux;
            }
        }
    });

    div.appendChild(maxTxt);
    div.appendChild(max);
    div.appendChild(eur);
}

/**
 * Fun��o para desenho especifico de valores de espa�o
 * @param {type} div div onde colocar os valores
 * @param {type} min valor m�nimo
 * @param {type} max valor m�ximo
 */
function semanticaEspaco(div, min, max) {
    var pattern = /[1-9]|[1-9][0-9]+/;
    var area = document.createElement("OUTPUT");
    area.value = "m2";
    var area1 = document.createElement("OUTPUT");
    area1.value = "m2";

    var minTxt = document.createElement("OUTPUT");
    minTxt.value = "Mínimo: ";

    var maxTxt = document.createElement("OUTPUT");
    maxTxt.value = "   Máximo: ";

    var space = document.createElement("br");

    min.addEventListener("change", function() {
        if (!pattern.test(min.value)) {
            min.value = "";
        } else if (min.value === "") {
            min.value === "";
        } else if (+min.value < min.name) {
            min.value = min.name;
        } else if ((max.value !== "") && (+min.value > +min.value)) {
            min.value = +max.value - 1;
        } else {
            min.value = this.value;

            if ((max.value !== "") && +max.value < +min.value) {
                var aux = max.value;

                max.value = min.value;
                min.value = aux;
            }
        }
    }
    );

    div.appendChild(minTxt);
    div.appendChild(min);
    div.appendChild(area1);

    max.addEventListener("change", function() {
        if (!pattern.test(max.value)) {
            max.value = "";
        } else if (max.value === "") {
            max.value === "";
        } else if (+max.value > max.name) {
            max.value = max.name;
        } else if ((min.value !== "") && +max.value < +min.value) {
            max.value = +min.value + 1;
        } else {
            max.value = this.value;

            if ((min.value !== "") && +min.value > +max.value) {
                var aux = max.value;
                max.value = min.value;
                min.value = aux;
            }
        }
    });

    div.appendChild(maxTxt);
    div.appendChild(max);
    div.appendChild(area);
}

/**
 * Fun��o que efetua o pedido Ajax para obter o valor m�nimo da faceta
 * @param {type} textbox para configurar valor m�nimo
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getValorMinimoFacetaAJAX(textbox, name, id) {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxMinMax[id] = new XMLHttpRequest();
    } else {
        ajaxMinMax[id] = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (ajaxMinMax[id]) {
        ajaxMinMax[id].onreadystatechange = function() {
            getValoresMinimoFacetaJSON(textbox, name, id);
        };
        ajaxMinMax[id].open("GET", encodeURI(urlValorMinimoFaceta + name), true);
        ajaxMinMax[id].send(null);
    }
}

/**
 * Fun��o que constroi o objeto JSON para extrair o valor m�nimo da faceta
 * @param {type} textbox para configurar valor m�nimo
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getValoresMinimoFacetaJSON(textbox, name, id) {
    if (ajaxMinMax[id].readyState == 4 && ajaxMinMax[id].status == 200) {
        var text = ajaxMinMax[id].responseText;
        var data = [];
        var data = JSON.parse(text);

        textbox.name = data.min;
    }
}

/**
 * Fun��o que efetua o pedido Ajax para obter o valor m�ximo da faceta
 * @param {type} textbox para configurar valor m�ximo
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getValorMaxFacetaAJAX(textbox, name, id) {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxMinMax[id] = new XMLHttpRequest();
    } else {
        ajaxMinMax[id] = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (ajaxMinMax[id]) {
        ajaxMinMax[id].onreadystatechange = function() {
            getValoresMaxFacetaJSON(textbox, name, id);
        };
        ajaxMinMax[id].open("GET", encodeURI(urlValorMaximoFaceta + name), true);
        ajaxMinMax[id].send(null);
    }

}

/**
 * Fun��o que constroi o objeto JSON para extrair o valor m�ximo da faceta
 * @param {type} textbox para configurar valor m�nimo
 * @param {type} name nome da faceta
 * @param {type} id id da faceta
 */
function getValoresMaxFacetaJSON(textbox, name, id) {
    if (ajaxMinMax[id].readyState == 4 && ajaxMinMax[id].status == 200) {
        var text = ajaxMinMax[id].responseText;
        var data = [];
        var data = JSON.parse(text);

        textbox.name = data.max;
    }
}

/**
 * Fun��o que efetua o pedido final de busca, construindo o "url" de pesquisa dos valores filtrados pelo utilizador
 * @param {type} divOutput div para colocar a tabela com o output
 */
function search(divOutput) {
    var result = "";
    var divFacetas = document.getElementById('divFacetas');

    var facetas = $(divFacetas).children("div");
    var i = 0;

    var list = [];
    var continuos = [];
    var values = [];
    for (i = 0; i < facetas.length; i++) {
        var divValores = document.getElementById("valores_" + facetas[i].id);
        var hasTextBoxes = 0;

        var name = $(facetas[i]).find("span").contents().filter(function() {
            return this.nodeType == 3;
        }).text();

        $.each($(divValores).find("input[type='textbox']"), function() {
            hasTextBoxes = 1;
        });

        if (name == "fotos") {
            if (facetas[i].children[0].checked == true) {
                list.push("fotos");
            }
        } else {

            if (hasTextBoxes == 0) {
                var facetaHasFilter = false;

                $.each($(divValores).find("input[type='checkbox']:checked"), function() {
                    facetaHasFilter = true;
                });

                if (facetaHasFilter == false) {
                    //Extract facet name
                    list.push(name);
                    result += name + "=[";

                    $.each($(divValores).find("input[type='checkbox']"), function() {
                        result += $(this).attr('name') + ",";
                    });

                    result = result.substring(0, result.length - 1); //retirar , a mais
                    result += "]&";
                } else {
                    list.push(name);
                    result += name + "=[";

                    $.each($(divValores).find("input[type='checkbox']:checked"), function() {
                        result += $(this).attr('name') + ",";
                    });

                    result = result.substring(0, result.length - 1); //retirar , a mais
                    result += "]&";
                }

            } else {
                list.push(name);
                continuos.push(name);

                var textboxes = divValores.getElementsByTagName("INPUT");
                var j = 0;
                for (j = 0; j < textboxes.length; j++) {
                    if (j == 0) { //min
                        if (textboxes[j].value == "") { //caso não tenha digitado valor
                            values.push(textboxes[j].name);
                        } else {
                            values.push(textboxes[j].value);
                        }
                    }
                    if (j == 1) { //max
                        if (textboxes[j].value == "") { //caso não tenha digitado valor
                            values.push(textboxes[j].name);
                        } else {
                            values.push(textboxes[j].value);
                        }
                    }
                }
            }
        }
    }

    result = result.substring(0, result.length - 1);

    getResultsAJAX(divOutput, result, list, continuos, values);
}

/**
 * Fun��o que utiliza o url criado na fun��o "search" para efetuar o pedido Ajax para os resultados pretendidos
 * @param {type} divOutput div para colocar a tabela com o output
 * @param {type} result url para o resultado
 * @param {type} list lista com os nomes das facetas a filtrar
 * @param {type} continuos lista com os nomes das facetas continuas a filtrar
 * @param {type} values valores a filtrar
 */
function getResultsAJAX(divOutput, result, list, continuos, values) {
    if (window.XMLHttpRequest) { //browsers recentes
        ajaxSearch[0] = new XMLHttpRequest();
    } else {
        ajaxSearch[0] = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (ajaxSearch[0]) {
        ajaxSearch[0].onreadystatechange = function() {
            getResultsJSON(divOutput, list, continuos, values);
        };
        ajaxSearch[0].open("GET", encodeURI(url + result), true);
        ajaxSearch[0].send(null);
    }
}

/**
 * Fun��o que constroi o objeto JSON para extrair os resultados finais da pesquisa
 * @param {type} divOutput div para colocar a tabela com o output
 * @param {type} result url para o resultado
 * @param {type} continuos lista com os nomes das facetas continuas a filtrar
 * @param {type} values valores a filtrar
 */
function getResultsJSON(divOutput, list, continuos, values) {
    if (ajaxSearch[0].readyState == 4 && ajaxSearch[0].status == 200) {
        var text = ajaxSearch[0].responseText;
        var data = JSON.parse(text);

        writeOutput(divOutput, data, list, continuos, values);
    }
}

/**
 * Fun��o que cria a tabela final em HTML com os resultados pretendidos
 * @param {type} divOutput div para colocar a tabela com o output
 * @param {type} data string com os resultados
 * @param {type} list lista com os nomes das facetas a filtrar
 * @param {type} continuos lista com os nomes das facetas continuas a filtrar
 * @param {type} values valores a filtrar
 */
function writeOutput(divOutput, data, list, continuos, values) {
    var table = document.createElement("table");
    table.border = "1";
    table.id = "table";
    table.className = "table";
    
    //HEADINGS
    var i = 0;
    var thead = document.createElement("thead");
    thead.id = "thead";
    var tr = document.createElement("tr");
    for (i = 0; i < list.length; i++) {
        var th = document.createElement("th");
        var text = document.createTextNode(list[i]);
        th.appendChild(text);
        tr.appendChild(th);
        thead.appendChild(tr);
    }

    table.appendChild(thead);
    var tbody = document.createElement("tbody");
    tbody.id = "tbody";
    i = 0;
    
    //VALUES
    for (i = 0; i < data.length; i++) {
        var writeLine = isToWrite(list, continuos, data, values, i);
        var j = 0;
        if (writeLine) {
            var tr2 = document.createElement("tr");
            for (j = 0; j < list.length; j++) {
                var td = document.createElement("td");
                if (list[j] == "fotos") {
                    var url = data[i].fotos;
                    var img = document.createElement("IMG");
                    img.id = "img";
                    img.src = url;
                    img.style = "width: 5%; height: 5%";
                    td.appendChild(img);
                } else {
                    var value = $(data[i]).attr(list[j]);
                    var text = document.createTextNode(value);
                    td.appendChild(text);
                }
                tr2.appendChild(td);
                tbody.appendChild(tr2);
                table.appendChild(tbody);
            }
        }
    }

    var output = document.getElementById(divOutput);
    if (output === null) {
        output = document.getElementsByClassName(divOutput);
        
        while (output[0].firstChild) {
            output[0].removeChild(output[0].firstChild);
        }
        output[0].appendChild(table);
        
    } else {    
        while (output.firstChild) {
            output.removeChild(output.firstChild);
        }
        output.appendChild(table);
    }
}

/**
 * Verifica��o se o resultado se encontra dentro dos padr�es m�nimo e m�ximo dos filtros nas facetas cont�nuas
 * @param {type} data string com os resultados
 * @param {type} list lista com os nomes das facetas a filtrar
 * @param {type} continuos lista com os nomes das facetas continuas a filtrar
 * @param {type} values valores a filtrar
 * @param {type} i posi��o do vetor de valores
 * @returns {Boolean} true se for para escrever
 */
function isToWrite(list, continuos, data, values, i) {
    var writeLine = true;
    var j = 0;
    for (j = 0; j < list.length; j++) {
        //Verificar se tem facetas continuas
        if ($.inArray(list[j], continuos) != -1) {
            var element = $.inArray(list[j], continuos);
            if (element == 0) {
                var min = values[0];
                var max = values[1];
            } else {
                var min = values[element * 2];
                var max = values[(element * 2) + 1];
            }
            var value = $(data[i]).attr(list[j]);
            if (!(+value > +min && +value < +max)) {
                writeLine = false;
                return writeLine;
            }
        }
    }

    return writeLine;
}