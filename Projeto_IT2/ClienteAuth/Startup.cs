﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClienteAuth.Startup))]
namespace ClienteAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
