﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClienteAuth.Models
{
    public class Gps
    {
        [DisplayName("Latitude:")]
        [Range(-90.00, 90.00, ErrorMessage= "Valor da Latitude tem de ser entre -90º e 90º")]
        public decimal? Latitude { get; set; }

        [DisplayName("Longitude:")]
        [Range(-180.00, 180.00, ErrorMessage = "Valor da Longitude tem de ser entre -90º e 90º")]
        public decimal? Longitude { get; set; }

    }
}