﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ClienteAuth.Models
{
    public class Localizacao
    {
        [DisplayName("Localização")]
        public int LocalizacaoID { get; set; }
        [DisplayName("Localização")]
        public string Nome { get; set; }

    }
}