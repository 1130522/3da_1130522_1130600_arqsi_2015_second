﻿using System.Collections.Generic;
using System;
using System.ComponentModel;

namespace ClienteAuth.Models
{
    public class Imovel
    {
        public int ImovelID { get; set; }
        [DisplayName("Nome do Imóvel")]
        public string Nome { get; set; }
        [DisplayName("Área (m2)")]
        public float Area { get; set; }
        [DisplayName("Preço (€)")]
        public float Preco { get; set; }
        [DisplayName("Localização")]
        public string Localizacao { get; set; }
        [DisplayName("Tipo de Imóvel")]
        public string TipoImovel { get; set; }
        [DisplayName("Proprietário")]
        public string Utilizador { get; set; }
        public string Url { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }

        [DisplayName("Foto")]
        public Foto Foto { get; set; }

        [DisplayName("Coordenadas GPS:")]
        public Gps Gps { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public int TipoImovelID { get; set; }
        [DisplayName("Localização")]
        public int LocalizacaoID { get; set; }
        [DisplayName("Foto")]
        public int FotoID { get; set; }
    }
}