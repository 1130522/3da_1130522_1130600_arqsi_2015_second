﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ClienteAuth.Models
{
    public class TipoImovel
    {
        [DisplayName("Tipo do Imóvel")]
        public int TipoImovelID { get; set; }
        [DisplayName("Tipo do Imóvel")]
        public String Nome { get; set; }

        public virtual ICollection<Alerta> Alertas { get; set; }
    }
}