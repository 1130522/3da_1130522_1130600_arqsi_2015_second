﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClienteAuth.Models
{
    public enum TipoAnuncio { Venda, Aluguer, Permuta, Compra }
    public class Anuncio
    {
        [DisplayName("Nome do Anúncio")]
        public int AnuncioID { get; set; }
        [DisplayName("Nome do Anúncio")]
        public string Nome { get; set; }
        [DisplayName("Descrição do Anúncio")]
        public string Descricao { get; set; }
        [DisplayName("Data de criação")]
        [DataType(DataType.Date)]
        public DateTime DataCriacao { get; set; }
        [DisplayName("Tipo do Anúncio")]
        public TipoAnuncio TipoAnuncio { get; set; }

        public int ImovelID { get; set; }
        public string ImovelUtilizador { get; set; }
        public string ImovelNome { get; set; }
        public string ImovelUrl { get; set; }
        public Imovel Imovel { get; set; }
     
    }
}