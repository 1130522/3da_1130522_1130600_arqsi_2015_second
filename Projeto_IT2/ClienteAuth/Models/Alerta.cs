﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ClienteAuth.Models
{

    public class Alerta
    {
        public int AlertaID { get; set; }
        public String Nome { get; set; }

        [DisplayName("Preço Mínimo (€)")]
        public float PrecoMin { get; set; }

        [DisplayName("Preço Máximo (€)")]
        public float PrecoMax { get; set; }
        public String Utilizador { get; set; }

        [DisplayName("Localização")]
        public int LocalizacaoID { get; set; }

        [DisplayName("Tipo de Imóvel")]
        public int TipoImovelID { get; set; }
        public string LocalizacaoNome { get; set; }
        public string TipoImovelNome { get; set; }
        public string TipoAnuncioNome { get; set; }
        [DisplayName("Tipo de Anúncio")]
        public TipoAnuncio TipoAnuncio { get; set; }
        public TipoImovel TipoImovel { get; set; }
        public Localizacao Localizacao { get; set; }
  
    

      
    }
}