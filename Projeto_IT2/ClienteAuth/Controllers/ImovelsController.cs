﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClienteAuth.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ClienteAuth.Models;

namespace ClienteAuth.Controllers
{
    public class ImovelsController : Controller
    {

        // GET: Imovels
        public async Task<ActionResult> Index()
        {
            var cliente = WebApiHttpClient.GetClient();

            HttpResponseMessage response = await cliente.GetAsync("api/Imovels");
            if (response.IsSuccessStatusCode)

            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis = JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);
                return View(imoveis);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }


        }
        // GET: Imovels/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Imovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Imovel>(content);
                if (i == null) return HttpNotFound();
                return View(i);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // GET: Imovels/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Localizacaos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var Localizacoes = JsonConvert.DeserializeObject<IEnumerable<Localizacao>>(content);
                ViewBag.LocalizacaoID = new SelectList(Localizacoes, "LocalizacaoID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            var client2 = WebApiHttpClient.GetClient();
            HttpResponseMessage response2 = await client2.GetAsync("api/TipoImovels");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var TiposImoveis = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content2);
                ViewBag.TipoImovelID = new SelectList(TiposImoveis, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            return View();
        }

        // POST: Imovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ImovelID,Nome,Area,Preco,LocalizacaoID,TipoImovelID,Foto,Utilizador,Gps")] Imovel imovel, [Bind(Include = "Url")] Foto foto)
        {

            try
            {
                //ImovelID, Nome, Area, Preco, LocalizacaoID, TipoImovelID, FotoID, 
                //Utilizador, GPS_Latitude, GPS_Longitude

                //Associação do utilizador logado ao imóvel
                imovel.Utilizador = WebApiHttpClient.getToken().Username;

                var cliente = WebApiHttpClient.GetClient();
                string beforeJSON = JsonConvert.SerializeObject(imovel);
                
                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");

                var response = await cliente.PostAsync("api/Imovels", content);

                if (response.IsSuccessStatusCode) {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }

             }
            catch {
                    return Content("Ocorreu um erro.");
            }

    }

        // GET: Imovels/Edit/5
        public async  Task<ActionResult> Edit(int? id)
        {
            //Criação das ViewBags para as DropDownLists
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Localizacaos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var Localizacoes = JsonConvert.DeserializeObject<IEnumerable<Localizacao>>(content);
                ViewBag.LocalizacaoID = new SelectList(Localizacoes, "LocalizacaoID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            var client2 = WebApiHttpClient.GetClient();
            HttpResponseMessage response2 = await client2.GetAsync("api/TipoImovels");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var TiposImoveis = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content2);
                ViewBag.TipoImovelID = new SelectList(TiposImoveis, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var cliente3 = WebApiHttpClient.GetClient();
            HttpResponseMessage resposta3 = await cliente3.GetAsync("api/Imovels/" + id);
            if (resposta3.IsSuccessStatusCode)
            {
                string content3 = await resposta3.Content.ReadAsStringAsync();
                var imovel = JsonConvert.DeserializeObject<Imovel>(content3);
                if(imovel == null)
                    return HttpNotFound();

                return View(imovel);
            }
                return Content("Ocorreu um erro: "+ resposta3.StatusCode);
         }

        // POST: Imovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ImovelID,Nome,Area,Preco,LocalizacaoID,TipoImovelID,FotoID,Utilizador,Longitude,Latitude")] Imovel imovel, [Bind(Include = "Url")] Foto foto)
        {
            try {
                Gps gps = new Gps() {
                    Latitude = imovel.Latitude,
                    Longitude = imovel.Longitude
                };

                imovel.Utilizador = WebApiHttpClient.getToken().Username;
                imovel.Gps = gps;
                imovel.Foto = foto;
           
                var cliente = WebApiHttpClient.GetClient();
                string beforeJSON = JsonConvert.SerializeObject(imovel);
                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await cliente.PutAsync("api/Imovels/" + imovel.ImovelID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch 
            {
                return Content("Ocorreu um erro.");
            }
           
        }

        // GET: Imovels/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Imovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Imovel>(content);
                if (i == null)
                    return HttpNotFound();
                return View(i);
            }

            return Content("Ocorreu um erro:" + response.StatusCode);
        }

        // POST: Imovels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var cliente = WebApiHttpClient.GetClient();
                var resposta = await cliente.DeleteAsync("api/Imovels/" + id);
                if (resposta.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + resposta.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
