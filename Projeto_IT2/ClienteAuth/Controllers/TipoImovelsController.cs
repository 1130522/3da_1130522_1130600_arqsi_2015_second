﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClienteAuth.Models;
using System.Threading.Tasks;
using System.Net.Http;
using ClienteAuth.Helpers;
using Newtonsoft.Json;

namespace ClienteAuth.Controllers
{
    //[Authorize(Roles = "Administrador")]
    public class TipoImovelsController : Controller
    {
       // private ImobiliariaContext db = new ImobiliariaContext();

        // GET: TipoImovels
        
             public async Task<ActionResult> Index() {

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                return View(tipoImovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }


        // GET: TipoImovels/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var timovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (timovel == null)
                    return HttpNotFound();
                return View(timovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: TipoImovels/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var timovel = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                ViewBag.TipoImovelID = new SelectList(timovel, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            return View();
        }

        // POST: TipoImovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> Create([Bind(Include = "TipoImovelID,Nome")] TipoImovel timovel)
        {
           

            try
            {
                var client = WebApiHttpClient.GetClient();
                string beforeJSON = JsonConvert.SerializeObject(timovel);
                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/TipoImovels", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: TipoImovels/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null)
                    return HttpNotFound();
                return View(tipoImovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
           
        }

        // POST: TipoImovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TipoImovelID,Nome")] TipoImovel tipoImovel)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string editoraJSON = JsonConvert.SerializeObject(tipoImovel);
                HttpContent content = new StringContent(editoraJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/TipoImovels/" + tipoImovel.TipoImovelID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: TipoImovels/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovels/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var timovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (timovel == null)
                    return HttpNotFound();
                return View(timovel);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: TipoImovels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/TipoImovels/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

       
    }
}
