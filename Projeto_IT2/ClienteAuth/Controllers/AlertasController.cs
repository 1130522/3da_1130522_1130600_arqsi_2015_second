﻿using ClienteAuth.Helpers;
using ClienteAuth.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ClienteAuth.Controllers
{
    public class AlertasController : Controller
    {
        // GET: Alertas
        public async Task<ActionResult> Index()
        {
            string username = "";
            var cliente = WebApiHttpClient.GetClient();
            if (WebApiHttpClient.getToken() != null)
            {
                username = WebApiHttpClient.getToken().Username;
            }           
            HttpResponseMessage response = await cliente.GetAsync("api/Alertas?username=" + username);
            if (response.IsSuccessStatusCode)

            {
                string content = await response.Content.ReadAsStringAsync();
                var alertas = JsonConvert.DeserializeObject<IEnumerable<Alerta>>(content);
                return View(alertas);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Imovels/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Alertas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Alerta>(content);
                if (i == null) return HttpNotFound();
                return View(i);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // GET: Alertas/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Localizacaos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var Localizacoes = JsonConvert.DeserializeObject<IEnumerable<Localizacao>>(content);
                ViewBag.LocalizacaoID = new SelectList(Localizacoes, "LocalizacaoID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            var client2 = WebApiHttpClient.GetClient();
            HttpResponseMessage response2 = await client2.GetAsync("api/TipoImovels");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var TiposImoveis = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content2);
                ViewBag.TipoImovelID = new SelectList(TiposImoveis, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AlertaID,Nome,PrecoMin,PrecoMax,Utilizador,LocalizacaoID,TipoImovelID")] Alerta alerta)
        {
            try
            {
                //Associação do utilizador logado ao alerta
                alerta.Utilizador = WebApiHttpClient.getToken().Username;

                var cliente = WebApiHttpClient.GetClient();
                string beforeJSON = JsonConvert.SerializeObject(alerta);

                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");

                var response = await cliente.PostAsync("api/Alertas", content);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }

            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }

        // GET: Alertas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            //Criação das ViewBags para as DropDownLists
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Localizacaos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var Localizacoes = JsonConvert.DeserializeObject<IEnumerable<Localizacao>>(content);
                ViewBag.LocalizacaoID = new SelectList(Localizacoes, "LocalizacaoID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            var client2 = WebApiHttpClient.GetClient();
            HttpResponseMessage response2 = await client2.GetAsync("api/TipoImovels");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var TiposImoveis = JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content2);
                ViewBag.TipoImovelID = new SelectList(TiposImoveis, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var cliente3 = WebApiHttpClient.GetClient();
            HttpResponseMessage resposta3 = await cliente3.GetAsync("api/Alertas/" + id);
            if (resposta3.IsSuccessStatusCode)
            {
                string content3 = await resposta3.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content3);
                if (alerta == null)
                    return HttpNotFound();

                return View(alerta);
            }
            return Content("Ocorreu um erro: " + resposta3.StatusCode);
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AlertaID,Nome,PrecoMin,PrecoMax,Utilizador,LocalizacaoID,TipoImovelID")] Alerta alerta)
        {
            try
            {
                var cliente = WebApiHttpClient.GetClient();
                string beforeJSON = JsonConvert.SerializeObject(alerta);
                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await cliente.PutAsync("api/Alertas/" + alerta.AlertaID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Alertas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Alertas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Alerta>(content);
                if (i == null)
                    return HttpNotFound();
                return View(i);
            }

            return Content("Ocorreu um erro:" + response.StatusCode);
        }

        // POST: Alertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var cliente = WebApiHttpClient.GetClient();
                var resposta = await cliente.DeleteAsync("api/Alertas/" + id);
                if (resposta.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + resposta.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

    }
}