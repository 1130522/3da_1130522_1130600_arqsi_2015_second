﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ClienteAuth.Models;
using System.Net.Http;
using ClienteAuth.Helpers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace ClienteAuth.Controllers
{
    public class AnunciosController : Controller
    {

        // GET: Anuncios
        public async Task<ActionResult> Index()
        {
            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Anuncios");
            if (response.IsSuccessStatusCode)

            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncios = JsonConvert.DeserializeObject<IEnumerable<Anuncio>>(content);
                return View(anuncios);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Anuncios/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Anuncio>(content);
                if (i == null) return HttpNotFound();
                return View(i);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncios/Create
        public async Task<ActionResult> Create()
        {
            string username = "";
            var client = WebApiHttpClient.GetClient();
            if(WebApiHttpClient.getToken() != null)
            {
               username = WebApiHttpClient.getToken().Username;
            }
            else
            {
                return RedirectToAction("Index");
            }

            HttpResponseMessage response = await client.GetAsync("api/Imovels?username=" + username);
            if (response.IsSuccessStatusCode)
            {
                string content1 = await response.Content.ReadAsStringAsync();

                var listaImoveis = JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content1); 
                ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome");

                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // POST: Anuncios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task <ActionResult> Create([Bind(Include = "AnuncioID,Nome,Descricao,DataCriacao,TipoAnuncio,ImovelID")] Anuncio anuncio)
        {

            var client = WebApiHttpClient.GetClient();
            
            var username = WebApiHttpClient.getToken().Username;
            try
            {
                //force do utilizador
                
                string beforeJSON = JsonConvert.SerializeObject(anuncio);

                HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncios", content);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }

            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Anuncios/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovels");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var listaImoveis1 = JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);
                string username = WebApiHttpClient.getToken().Username;
                ViewBag.ImovelID = new SelectList(listaImoveis1.Where(i => i.Utilizador == username), "ImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            var client2 = WebApiHttpClient.GetClient();
            HttpResponseMessage response2 = await client2.GetAsync("api/Anuncios/" + id);

            if (response.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<Anuncio>(content2);
                if (anuncio == null)
                {
                    return HttpNotFound();
                }
           return View(anuncio);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Anuncios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AnuncioID,Nome,Descricao,DataCriacao,TipoAnuncio,ImovelID")] Anuncio anuncio)
        {
            try{
                var testeTipo = anuncio.TipoAnuncio.ToString();
                var testeImovelId = anuncio.ImovelID;
                anuncio.ImovelUtilizador = WebApiHttpClient.getToken().Username;
                var cliente = WebApiHttpClient.GetClient();
            string beforeJSON = JsonConvert.SerializeObject(anuncio);

            HttpContent content = new StringContent(beforeJSON, System.Text.Encoding.Unicode, "application/json");
            var response = await cliente.PutAsync("api/Anuncios/" + anuncio.AnuncioID, content);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro" + response.StatusCode);
                }
            }

            catch
            {
                return Content("Ocorreu um erro");
            }
            
           
        }

        // GET: Anuncios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cliente = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await cliente.GetAsync("api/Anuncios/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var i = JsonConvert.DeserializeObject<Anuncio>(content);
                if (i == null)
                    return HttpNotFound();
                return View(i);
            }

            return Content("Ocorreu um erro:" + response.StatusCode);
        }

        // POST: Anuncios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id) { 
         try
            {
                var cliente = WebApiHttpClient.GetClient();
                var resposta = await cliente.DeleteAsync("api/Anuncios/" + id);
                if (resposta.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + resposta.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
         }
    }
    }