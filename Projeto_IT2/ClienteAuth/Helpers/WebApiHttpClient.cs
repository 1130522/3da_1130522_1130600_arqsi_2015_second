﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace ClienteAuth.Helpers
{
    public class WebApiHttpClient
    {
        public const string WebApiBaseAddress = "https://localhost:44305/";

        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(WebApiBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new
            System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            var session = HttpContext.Current.Session;
            if (session["token"] != null)
            {
                TokenResponse tokenResponse = getToken();
                client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("bearer", tokenResponse.AccessToken);
                //new AuthenticationHeaderValue("cliente", tokenResponse.Role);
            }

            return client;
        }

        public static void storeToken(TokenResponse token)
        {
            var session = HttpContext.Current.Session;
            session["token"] = token;
        }

        public static TokenResponse getToken()
        {
            var session = HttpContext.Current.Session;
            return (TokenResponse)session["token"];
        }

        public static void removeToken()
        {
            var session = HttpContext.Current.Session;
            session.Remove("token");
        }


    }
}