﻿using Library.DAL.Interface;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public class LocalizacoesRepository : ILocalizacoesRepository
    {
        public ApplicationDbContext context;

        public LocalizacoesRepository()
        {
            context = new ApplicationDbContext();
        }

        public IEnumerable<Localizacao> GetData()
        {
            var localizacoes = context.Localizacoes.ToList();
            return localizacoes;
        }

        public Task<Localizacao> GetData(int id)
        {
            return context.Localizacoes.FindAsync(id);
        }

        public async Task<Localizacao> Create(Localizacao local)
        {
            context.Localizacoes.Add(local);
            await context.SaveChangesAsync();
            return local;
        }

        public async Task<bool> Delete(int id)
        {
            var local = context.Localizacoes.Find(id);
            if (local != null)
            {
                context.Localizacoes.Remove(local);
                await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }


        public async Task<bool> Update(int id, Localizacao imo)
        {
            var imovel = context.Localizacoes.Find(id);
            if (imo != null)
            {
                imovel.Nome = imo.Nome;
                try
                {
                    await context.SaveChangesAsync();
                    return true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    return false;
                }
            }
            else return false;
        }
    }
}
