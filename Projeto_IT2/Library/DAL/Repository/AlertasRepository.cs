﻿using Library.DAL.Interface;
using Library.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public class AlertasRepository : IAlertasRepository
    {
        public ApplicationDbContext context;
        public AlertasRepository()
        {
            context = new ApplicationDbContext();
        }

        public IEnumerable<Alerta> GetData()
        {
            var alertas = context.Alertas.ToList();
            return alertas;
        }

        public Task<Alerta> GetData(int id)
        {
            return context.Alertas.FindAsync(id);
        }

        public IEnumerable<Alerta> GetDataByUser(string username)
        {
            var alertas = context.Alertas.ToList().Where(i => i.Utilizador == username);
            return alertas;
        }

        public async Task<Alerta> Create(Alerta alert)
        {
            context.Alertas.Add(alert);
            await context.SaveChangesAsync();
            return alert;
        }

        public async Task<bool> Delete(int id)
        {
            var alert = context.Alertas.Find(id);
            if (alert != null)
            {
                context.Alertas.Remove(alert);
               await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }
       
        public async Task<bool> Update(int id, Alerta a)
        {
            var alerta = context.Alertas.Find(id);
            if (a != null)
            {
                alerta.Nome = a.Nome;
                alerta.Localizacao = a.Localizacao;
                alerta.PrecoMin = a.PrecoMin;
                alerta.PrecoMax = a.PrecoMax;
                alerta.TipoImovelID = a.TipoImovelID;
                alerta.TipoAnuncio = a.TipoAnuncio;
                alerta.LocalizacaoID = a.LocalizacaoID;
                alerta.Utilizador = a.Utilizador;
                try
                {
                await context.SaveChangesAsync();
                }
                catch(DbUpdateConcurrencyException)
                {

                }
                return true;
            }
            else return false;
        }
    }
}
