﻿using Library.DAL.Interface;
using Library.Models;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Library.DAL.Repository
{
    public class ImoveisRepository : IImoveisRepository
    {
        public ApplicationDbContext context;

        public ImoveisRepository()
        {
            context = new ApplicationDbContext();
        }

        public IEnumerable<Imovel> GetData()
        {
            var imoveis = context.Imoveis.ToList();
            return imoveis;
        }

        public IEnumerable<Imovel> GetImovelByUser(string username)
        {
            var imoveis = context.Imoveis.ToList().Where(i => i.Utilizador == username);
            return imoveis;
        }

        public Task<Imovel> GetData(int id)
        {
            return context.Imoveis.FindAsync(id);
        }

        public async Task<Imovel> Create(Imovel imovel)
        {
            context.Imoveis.Add(imovel);
            await context.SaveChangesAsync();
            return imovel;
        }

        public async Task<bool> Delete(int id)
        {
            var imovel = context.Imoveis.Find(id);
            if (imovel != null)
            {
                context.Imoveis.Remove(imovel);
                await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }


        public async Task<bool> Update(int id, Imovel imo)
        {
            var imovel = context.Imoveis.Find(id);
            if (imo != null)
            {
                imovel.Nome = imo.Nome;
                imovel.TipoImovelID = imo.TipoImovelID;
                imovel.Preco = imo.Preco;
                imovel.LocalizacaoID = imo.LocalizacaoID;
                imovel.GPS.Latitude = imo.GPS.Latitude;
                imovel.GPS.Longitude = imo.GPS.Longitude;
                imovel.Area = imo.Area;
                imovel.Foto.Url = imo.Foto.Url;
                try
                {
                    await context.SaveChangesAsync();
                    return true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    return false;
                }
            }
            else return false;
        }

    }
}
