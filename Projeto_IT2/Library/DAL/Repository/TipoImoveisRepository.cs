﻿using Library.DAL.Interface;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public class TipoImoveisRepository : ITipoImoveisRepository
    {
        public ApplicationDbContext context;

        public TipoImoveisRepository()
        {
            context = new ApplicationDbContext();
        }

        public async Task<TipoImovel> Create(TipoImovel timovel)
        {
            context.TipoImoveis.Add(timovel);
            await context.SaveChangesAsync();
            return timovel;
        }

        public async Task<bool> Delete(int id)
        {
            var timovel = context.TipoImoveis.Find(id);
            if (timovel != null)
            {
                context.TipoImoveis.Remove(timovel);
                await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }

        public async Task<bool> Update(int id, TipoImovel timovel)
        {
            var tipoimovel = context.TipoImoveis.Find(id);
            if (timovel != null)
            {
                tipoimovel.Nome = timovel.Nome;
                try
                {
                    await context.SaveChangesAsync();
                    return true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    return false;
                }
            }
            else return false;
        }

        public IEnumerable<TipoImovel> GetData()
        {
            var tipoimoveis = context.TipoImoveis.ToList();
            return tipoimoveis;
        }

        public Task<TipoImovel> GetData(int id)
        {
            return context.TipoImoveis.FindAsync(id);
        }

        public TipoImovel GetData(string nometipo)
        {
            return context.TipoImoveis.Find(nometipo);
        }
    }
}
