﻿using Library.DAL.Interface;
using Library.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public class FotosRepository : IFotosRepository
    {
        public ApplicationDbContext context;

        public FotosRepository()
        {
            context = new ApplicationDbContext();
        }

        public IEnumerable<Foto> GetData()
        {
            var fotos = context.Fotos.ToList();
            return fotos;
        }

        public Task<Foto> GetData(int id)
        {
            return context.Fotos.FindAsync(id);
        }

        public async Task<Foto> Create(Foto foto)
        {
            context.Fotos.Add(foto);
            await context.SaveChangesAsync();
            return foto;
        }

        public async Task<bool> Delete(int id)
        {
            var foto = context.Fotos.Find(id);
            if (foto != null)
            {
                context.Fotos.Remove(foto);
                await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }


        public async Task<bool> Update(int id, Foto f)
        {
            var foto = context.Fotos.Find(id);
            if (f != null)
            {
                foto.Url = f.Url;
                try
                {
                    await context.SaveChangesAsync();
                    return true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    return false;
                }
            }
            else return false;
        }
    }
}
