﻿using Library.DAL.Interface;
using Library.Models;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Library.DAL.Repository
{
    public class AnunciosRepository : IAnunciosRepository
    {
        public ApplicationDbContext context;
        public AnunciosRepository()
        {
            context = new ApplicationDbContext();
        }
        public async Task<Anuncio> Create(Anuncio anun)
        {
            context.Anuncios.Add(anun);
            await context.SaveChangesAsync();
            return anun;
        }

        public async Task<bool> Delete(int id)
        {
            var anuncio = context.Anuncios.Find(id);
            if (anuncio != null)
            {
                context.Anuncios.Remove(anuncio);
                await context.SaveChangesAsync();
                return true;
            }
            else return false;
        }
        public IEnumerable<Anuncio> GetData()
        {
            var anuncios = context.Anuncios.ToList();
            return anuncios;
        }
        public Task<Anuncio> GetData(int id)
        {
            return context.Anuncios.FindAsync(id);
        }
       
        public async Task<bool> Update(int id, Anuncio a)
        {
            var anuncio = context.Anuncios.Find(id);
            if (a != null)
            {
                anuncio.Nome = a.Nome;
                anuncio.DataCriacao = a.DataCriacao;
                anuncio.Descricao = a.Descricao;
                anuncio.ImovelID = a.ImovelID;
                anuncio.TipoAnuncio = a.TipoAnuncio;
                anuncio.AnuncioID = a.AnuncioID;    
                try
                {
                    await context.SaveChangesAsync();
                    return true;
                }
                catch(DbUpdateConcurrencyException)
                {
                    return false;
                }
               
            }
            else return false;
        }
    }
}
