﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface IFotosRepository
    {
        IEnumerable<Foto> GetData();
        Task<Foto> GetData(int id);
        Task<Foto> Create(Foto foto);
        Task<bool> Update(int id, Foto foto);
        Task<bool> Delete(int id);
    }
}
