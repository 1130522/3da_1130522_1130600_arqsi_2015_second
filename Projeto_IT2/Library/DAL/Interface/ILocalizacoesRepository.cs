﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface ILocalizacoesRepository
    {
        IEnumerable<Localizacao> GetData();
        Task<Localizacao> GetData(int id);
        Task<Localizacao> Create(Localizacao local);
        Task<bool> Update(int id, Localizacao local);
        Task<bool> Delete(int id);
    }
}
