﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface IAnunciosRepository
    {
        IEnumerable<Anuncio> GetData();
        Task<Anuncio> GetData(int id);
        Task<Anuncio> Create(Anuncio anun);
        Task<bool> Update(int id, Anuncio anun);
        Task<bool> Delete(int id);
    }
}
