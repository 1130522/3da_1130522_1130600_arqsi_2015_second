﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface IImoveisRepository
    {
        IEnumerable<Imovel> GetData();
        Task<Imovel> GetData(int id);
        IEnumerable<Imovel> GetImovelByUser(string username);
        Task<Imovel> Create(Imovel imovel);
        Task<bool> Update(int id, Imovel imovel);
        Task<bool> Delete(int id);
    }
}
