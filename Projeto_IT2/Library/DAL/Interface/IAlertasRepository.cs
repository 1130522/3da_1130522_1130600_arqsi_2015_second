﻿using Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface IAlertasRepository
    {
        IEnumerable<Alerta> GetData();
        Task<Alerta> GetData(int id);
        Task<Alerta> Create(Alerta alert);
        IEnumerable<Alerta> GetDataByUser(string username);
        Task<bool> Update(int id, Alerta alert);
        Task<bool> Delete(int id);
    }
}
