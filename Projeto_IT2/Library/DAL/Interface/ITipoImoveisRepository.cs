﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Interface
{
    public interface ITipoImoveisRepository
    {
        IEnumerable<TipoImovel> GetData();
        Task<TipoImovel> GetData(int id);
        TipoImovel GetData(string nometipo);
        Task<TipoImovel> Create(TipoImovel timovel);
        Task<bool> Update(int id, TipoImovel timovel);
        Task<bool> Delete(int id);
    }
}
