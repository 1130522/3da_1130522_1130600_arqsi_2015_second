﻿using System.Collections.Generic;
using System;
using System.ComponentModel;

namespace Library.Models
{
    public class Imovel
    {
        [DisplayName("Nome do Imóvel")]
        public int ImovelID { get; set; }
        [DisplayName("Nome do Imóvel")]
        public string Nome { get; set; }
        [DisplayName("Área (m2)")]
        public float Area { get; set; }
        [DisplayName("Preço (€)")]
        public float Preco { get; set; }
        [DisplayName("Localização")]
        public int LocalizacaoID { get; set; }
        [DisplayName("Tipo de Imóvel")]
        public int TipoImovelID { get; set; }
        public int FotoID { get; set; }
        [DisplayName("Vendedor")]
        public String Utilizador { get; set; }

        [DisplayName("Coordenadas GPS:")]
        public Gps GPS { get; set; }

        public virtual Localizacao Localizacao { get; set; }
        public virtual TipoImovel TipoImovel { get; set; }
        public virtual Foto Foto { get; set; }
        public virtual ICollection<Anuncio> Anuncios { get; set; }

    }
}