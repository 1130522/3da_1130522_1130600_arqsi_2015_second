﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class Localizacao
    {
        [DisplayName("Localização")]
        public int LocalizacaoID { get; set; }
        [DisplayName("Localização")]
        public string Nome { get; set; }

        public virtual ICollection<Alerta> Alertas { get; set; }
    }
}