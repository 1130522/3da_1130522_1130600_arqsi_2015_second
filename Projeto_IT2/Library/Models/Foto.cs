﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class Foto
    {
        public int FotoID { get; set; }
        [DisplayName("Fotografia")]
        public String Url { get; set; }
    }
}