﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class Gps
    {
        [Range(-90.00, 90.00, ErrorMessage= "Valor da Latitude tem de ser entre -90º e 90º")]
        public decimal Latitude { get; set; }

        [Range(-180.00, 180.00, ErrorMessage = "Valor da Longitude tem de ser entre -90º e 90º")]
        public decimal Longitude { get; set; }

    }
}