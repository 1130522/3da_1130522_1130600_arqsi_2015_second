namespace Imobiliaria_Arqsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Siga : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocalizacaoAlertas",
                c => new
                    {
                        Localizacao_LocalizacaoID = c.Int(nullable: false),
                        Alerta_AlertaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Localizacao_LocalizacaoID, t.Alerta_AlertaID })
                .ForeignKey("dbo.Localizacaos", t => t.Localizacao_LocalizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.Alertas", t => t.Alerta_AlertaID, cascadeDelete: true)
                .Index(t => t.Localizacao_LocalizacaoID)
                .Index(t => t.Alerta_AlertaID);
            
            CreateTable(
                "dbo.TipoImovelAlertas",
                c => new
                    {
                        TipoImovel_TipoImovelID = c.Int(nullable: false),
                        Alerta_AlertaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TipoImovel_TipoImovelID, t.Alerta_AlertaID })
                .ForeignKey("dbo.TipoImovels", t => t.TipoImovel_TipoImovelID, cascadeDelete: true)
                .ForeignKey("dbo.Alertas", t => t.Alerta_AlertaID, cascadeDelete: true)
                .Index(t => t.TipoImovel_TipoImovelID)
                .Index(t => t.Alerta_AlertaID);
            
            AddColumn("dbo.Alertas", "Nome", c => c.String());
            AddColumn("dbo.Alertas", "PrecoMin", c => c.Single(nullable: false));
            AddColumn("dbo.Alertas", "PrecoMax", c => c.Single(nullable: false));
            AddColumn("dbo.Alertas", "Utilizador", c => c.String());
            AddColumn("dbo.Alertas", "TipoAnuncio", c => c.Int(nullable: false));
            AddColumn("dbo.Imovels", "Utilizador", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TipoImovelAlertas", "Alerta_AlertaID", "dbo.Alertas");
            DropForeignKey("dbo.TipoImovelAlertas", "TipoImovel_TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.LocalizacaoAlertas", "Alerta_AlertaID", "dbo.Alertas");
            DropForeignKey("dbo.LocalizacaoAlertas", "Localizacao_LocalizacaoID", "dbo.Localizacaos");
            DropIndex("dbo.TipoImovelAlertas", new[] { "Alerta_AlertaID" });
            DropIndex("dbo.TipoImovelAlertas", new[] { "TipoImovel_TipoImovelID" });
            DropIndex("dbo.LocalizacaoAlertas", new[] { "Alerta_AlertaID" });
            DropIndex("dbo.LocalizacaoAlertas", new[] { "Localizacao_LocalizacaoID" });
            DropColumn("dbo.Imovels", "Utilizador");
            DropColumn("dbo.Alertas", "TipoAnuncio");
            DropColumn("dbo.Alertas", "Utilizador");
            DropColumn("dbo.Alertas", "PrecoMax");
            DropColumn("dbo.Alertas", "PrecoMin");
            DropColumn("dbo.Alertas", "Nome");
            DropTable("dbo.TipoImovelAlertas");
            DropTable("dbo.LocalizacaoAlertas");
        }
    }
}
