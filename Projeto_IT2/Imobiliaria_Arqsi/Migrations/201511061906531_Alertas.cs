namespace Imobiliaria_Arqsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alertas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LocalizacaoAlertas", "Localizacao_LocalizacaoID", "dbo.Localizacaos");
            DropForeignKey("dbo.LocalizacaoAlertas", "Alerta_AlertaID", "dbo.Alertas");
            DropForeignKey("dbo.TipoImovelAlertas", "TipoImovel_TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.TipoImovelAlertas", "Alerta_AlertaID", "dbo.Alertas");
            DropIndex("dbo.LocalizacaoAlertas", new[] { "Localizacao_LocalizacaoID" });
            DropIndex("dbo.LocalizacaoAlertas", new[] { "Alerta_AlertaID" });
            DropIndex("dbo.TipoImovelAlertas", new[] { "TipoImovel_TipoImovelID" });
            DropIndex("dbo.TipoImovelAlertas", new[] { "Alerta_AlertaID" });
            AddColumn("dbo.Alertas", "LocalizacaoID", c => c.Int(nullable: false));
            AddColumn("dbo.Alertas", "TipoImovelID", c => c.Int(nullable: false));
            CreateIndex("dbo.Alertas", "LocalizacaoID");
            CreateIndex("dbo.Alertas", "TipoImovelID");
            AddForeignKey("dbo.Alertas", "LocalizacaoID", "dbo.Localizacaos", "LocalizacaoID", cascadeDelete: true);
            AddForeignKey("dbo.Alertas", "TipoImovelID", "dbo.TipoImovels", "TipoImovelID", cascadeDelete: true);
            DropTable("dbo.LocalizacaoAlertas");
            DropTable("dbo.TipoImovelAlertas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TipoImovelAlertas",
                c => new
                    {
                        TipoImovel_TipoImovelID = c.Int(nullable: false),
                        Alerta_AlertaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TipoImovel_TipoImovelID, t.Alerta_AlertaID });
            
            CreateTable(
                "dbo.LocalizacaoAlertas",
                c => new
                    {
                        Localizacao_LocalizacaoID = c.Int(nullable: false),
                        Alerta_AlertaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Localizacao_LocalizacaoID, t.Alerta_AlertaID });
            
            DropForeignKey("dbo.Alertas", "TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.Alertas", "LocalizacaoID", "dbo.Localizacaos");
            DropIndex("dbo.Alertas", new[] { "TipoImovelID" });
            DropIndex("dbo.Alertas", new[] { "LocalizacaoID" });
            DropColumn("dbo.Alertas", "TipoImovelID");
            DropColumn("dbo.Alertas", "LocalizacaoID");
            CreateIndex("dbo.TipoImovelAlertas", "Alerta_AlertaID");
            CreateIndex("dbo.TipoImovelAlertas", "TipoImovel_TipoImovelID");
            CreateIndex("dbo.LocalizacaoAlertas", "Alerta_AlertaID");
            CreateIndex("dbo.LocalizacaoAlertas", "Localizacao_LocalizacaoID");
            AddForeignKey("dbo.TipoImovelAlertas", "Alerta_AlertaID", "dbo.Alertas", "AlertaID", cascadeDelete: true);
            AddForeignKey("dbo.TipoImovelAlertas", "TipoImovel_TipoImovelID", "dbo.TipoImovels", "TipoImovelID", cascadeDelete: true);
            AddForeignKey("dbo.LocalizacaoAlertas", "Alerta_AlertaID", "dbo.Alertas", "AlertaID", cascadeDelete: true);
            AddForeignKey("dbo.LocalizacaoAlertas", "Localizacao_LocalizacaoID", "dbo.Localizacaos", "LocalizacaoID", cascadeDelete: true);
        }
    }
}
