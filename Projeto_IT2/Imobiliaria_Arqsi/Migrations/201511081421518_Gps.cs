namespace Imobiliaria_Arqsi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Gps : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Imovels", "GPS_Latitude", c => c.Decimal(nullable: true, precision: 18, scale: 2));
            AddColumn("dbo.Imovels", "GPS_Longitude", c => c.Decimal(nullable: true, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Imovels", "GPS_Longitude");
            DropColumn("dbo.Imovels", "GPS_Latitude");
        }
    }
}
