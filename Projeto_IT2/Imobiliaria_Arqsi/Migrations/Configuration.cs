namespace Imobiliaria_Arqsi.Migrations
{
    using Library.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<Library.Models.ImobiliariaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Library.Models.ImobiliariaContext context)
        {

            if (!context.Roles.Any(r => r.Name == "Administrador"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Administrador" };
                manager.Create(role);
            }


            if (!context.Roles.Any(r => r.Name == "Cliente"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Cliente" };
                manager.Create(role);
            }


            if (!context.Users.Any(u => u.UserName == "admin@gmail.com"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "admin@gmail.com", Email = "admin@gmail.com" };
                manager.Create(user, "Abc123!");
                manager.AddToRole(user.Id, "Administrador");
            }

            var fotos = new List<Foto>
            {
                new Foto { Url="http://www.teste.com" }
            };
            fotos.ForEach(s => context.Fotos.AddOrUpdate(p => p.Url, s));
            context.SaveChanges();

            var localizacoes = new List<Localizacao>
            {
                new Localizacao { Nome ="Porto"},
                new Localizacao { Nome ="Lisboa"},
                new Localizacao { Nome ="Aveiro"},
                new Localizacao { Nome ="Coimbra"},
                new Localizacao { Nome="Guif�es"},
                new Localizacao { Nome="Faro"},
                new Localizacao { Nome="Alentejo"}
            };

            localizacoes.ForEach(l => context.Localizacoes.AddOrUpdate(p => p.Nome, l));
            context.SaveChanges();

        }
    }
}
