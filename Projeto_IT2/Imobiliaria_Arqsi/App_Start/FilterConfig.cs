﻿using System.Web;
using System.Web.Mvc;

namespace Imobiliaria_Arqsi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
