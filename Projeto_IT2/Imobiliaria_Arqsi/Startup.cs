﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Imobiliaria_Arqsi.Startup))]
namespace Imobiliaria_Arqsi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
