﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Library.Models;

namespace Imobiliaria_Arqsi.Controllers
{
    public class AlertasController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: Alertas
        public ActionResult Index()
        {
            var utilizador = User.Identity.Name;
            var alertas = db.Alertas.Include(a => a.Localizacao).Include(a => a.TipoImovel).Where(i => i.Utilizador == utilizador);

            return View(alertas.ToList());
        }

        // GET: Alertas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alertas/Create
        public ActionResult Create()
        {
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome");
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome");
            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlertaID,Nome,PrecoMin,PrecoMax,Utilizador,LocalizacaoID,TipoImovelID,TipoAnuncio")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                alerta.Utilizador = User.Identity.Name;

                if(alerta.PrecoMin > alerta.PrecoMax)
                {
                    ModelState.AddModelError("", "O preço mínimo não pode ser menor que o preço máximo.");
                    ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", alerta.LocalizacaoID);
                    ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", alerta.TipoImovelID);
                    return View(alerta);
                }

                if(String.ReferenceEquals(alerta.LocalizacaoID, "Escolha uma localização"))
                {
                    ModelState.AddModelError("", "Alerta o ser humano é básico");
                    ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", alerta.LocalizacaoID);
                    ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", alerta.TipoImovelID);
                    return View(alerta);
                }

                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", alerta.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", alerta.TipoImovelID);
            return View(alerta);
        }

        // GET: Alertas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", alerta.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", alerta.TipoImovelID);
            return View(alerta);
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlertaID,Nome,PrecoMin,PrecoMax,Utilizador,LocalizacaoID,TipoImovelID,TipoAnuncio")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", alerta.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", alerta.TipoImovelID);
            return View(alerta);
        }

        // GET: Alertas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
