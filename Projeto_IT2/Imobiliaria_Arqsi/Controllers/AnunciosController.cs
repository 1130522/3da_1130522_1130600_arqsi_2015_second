﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Library.Models;

namespace Imobiliaria_Arqsi.Controllers
{
    public class AnunciosController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: Anuncios
        public ActionResult Index()
        {
            var anuncios = db.Anuncios.Include(a => a.Imovel);
            return View(anuncios.ToList());
        }

        // GET: Anuncios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            return View(anuncio);
        }

        // GET: Anuncios/Create
        public ActionResult Create()
        {
            var utilizador = User.Identity.Name;
            var listaImoveis = db.Imoveis.Where(i => i.Utilizador == utilizador);
            ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome");
            return View();
        }

        // POST: Anuncios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnuncioID,Nome,Descricao,DataCriacao,TipoAnuncio,ImovelID")] Anuncio anuncio)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Anuncios.Add(anuncio);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException dex)
            {
                ModelState.AddModelError("", "Impossível criar anúcio. Terá de registar um imóvel primeiro.");
            }
            var utilizador = User.Identity.Name;
            var listaImoveis = db.Imoveis.Where(i => i.Utilizador == utilizador);
            ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome", anuncio.ImovelID);
            return View(anuncio);
        }

        // GET: Anuncios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            var utilizador = User.Identity.Name;
            var listaImoveis = db.Imoveis.Where(i => i.Utilizador == utilizador);
            ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome", anuncio.ImovelID);
            return View(anuncio);
        }

        // POST: Anuncios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnuncioID,Nome,Descricao,DataCriacao,TipoAnuncio,ImovelID")] Anuncio anuncio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(anuncio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var utilizador = User.Identity.Name;
            var listaImoveis = db.Imoveis.Where(i => i.Utilizador == utilizador);
            ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome");
            ViewBag.ImovelID = new SelectList(listaImoveis, "ImovelID", "Nome", anuncio.ImovelID);
            return View(anuncio);
        }

        // GET: Anuncios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            return View(anuncio);
        }

        // POST: Anuncios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Anuncio anuncio = db.Anuncios.Find(id);
            db.Anuncios.Remove(anuncio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}