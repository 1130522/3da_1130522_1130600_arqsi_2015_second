﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Library.Models;

namespace Imobiliaria_Arqsi.Controllers
{
    public class ImovelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Imovels
        public ActionResult Index()
        {
            var imoveis = db.Imoveis.Include(i => i.Foto).Include(i => i.Localizacao).Include(i => i.TipoImovel);
            return View(imoveis.ToList());
        }

        // GET: Imovels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // GET: Imovels/Create
        public ActionResult Create()
        {
            ViewBag.FotoID = new SelectList(db.Fotos, "FotoID", "Url");
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome");
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome");
            return View();
        }

        // POST: Imovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ImovelID,Nome,Area,Preco,LocalizacaoID,TipoImovelID,FotoID,Utilizador, Gps")] Imovel imovel, [Bind(Include= "Url")] Foto foto)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    db.Fotos.Add(foto);
                    db.SaveChanges();
                    imovel.Foto = foto;
                    imovel.FotoID = foto.FotoID;
                    imovel.Utilizador = User.Identity.Name;
                    db.Imoveis.Add(imovel);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException dex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Impossível criar imóvel. Contacte o administrador.");
            }

            ViewBag.FotoID = new SelectList(db.Fotos, "FotoID", "Url", imovel.FotoID);
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", imovel.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", imovel.TipoImovelID);
            return View(imovel);
        }
 

        // GET: Imovels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            ViewBag.FotoID = new SelectList(db.Fotos, "FotoID", "Url", imovel.FotoID);
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", imovel.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", imovel.TipoImovelID);
            return View(imovel);
        }

        // POST: Imovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ImovelID,Nome,Area,Preco,LocalizacaoID,TipoImovelID,FotoID,Utilizador")] Imovel imovel, [Bind(Include = "Url")] Foto foto)
        {
            try {
                if (ModelState.IsValid)
                {
                    db.Fotos.Add(foto);
                    db.SaveChanges();
                    imovel.Utilizador = User.Identity.Name;
                    imovel.Foto = foto;
                    imovel.FotoID = foto.FotoID;
                 
                    db.SaveChanges();
                    db.Entry(imovel).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException data)
            {
                ModelState.AddModelError("", "Impossível editar imóvel. Contacte o administrador.");
            }
            ViewBag.FotoID = new SelectList(db.Fotos, "FotoID", "Url", imovel.FotoID);
            ViewBag.LocalizacaoID = new SelectList(db.Localizacoes, "LocalizacaoID", "Nome", imovel.LocalizacaoID);
            ViewBag.TipoImovelID = new SelectList(db.TipoImoveis, "TipoImovelID", "Nome", imovel.TipoImovelID);
            return View(imovel);
        }

        // GET: Imovels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // POST: Imovels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imovel imovel = db.Imoveis.Find(id);
            db.Imoveis.Remove(imovel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
