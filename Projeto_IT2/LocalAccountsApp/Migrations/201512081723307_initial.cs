namespace LocalAccountsApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        AlertaID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        PrecoMin = c.Single(nullable: false),
                        PrecoMax = c.Single(nullable: false),
                        Utilizador = c.String(),
                        LocalizacaoID = c.Int(nullable: false),
                        TipoImovelID = c.Int(nullable: false),
                        TipoAnuncio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AlertaID)
                .ForeignKey("dbo.Localizacaos", t => t.LocalizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.TipoImovels", t => t.TipoImovelID, cascadeDelete: true)
                .Index(t => t.LocalizacaoID)
                .Index(t => t.TipoImovelID);
            
            CreateTable(
                "dbo.Localizacaos",
                c => new
                    {
                        LocalizacaoID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.LocalizacaoID);
            
            CreateTable(
                "dbo.TipoImovels",
                c => new
                    {
                        TipoImovelID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.TipoImovelID);
            
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        AnuncioID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                        DataCriacao = c.DateTime(nullable: false),
                        TipoAnuncio = c.Int(nullable: false),
                        ImovelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AnuncioID)
                .ForeignKey("dbo.Imovels", t => t.ImovelID, cascadeDelete: true)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Imovels",
                c => new
                    {
                        ImovelID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Area = c.Single(nullable: false),
                        Preco = c.Single(nullable: false),
                        LocalizacaoID = c.Int(nullable: false),
                        TipoImovelID = c.Int(nullable: false),
                        FotoID = c.Int(nullable: false),
                        Utilizador = c.String(),
                        GPS_Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GPS_Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ImovelID)
                .ForeignKey("dbo.Fotoes", t => t.FotoID, cascadeDelete: true)
                .ForeignKey("dbo.Localizacaos", t => t.LocalizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.TipoImovels", t => t.TipoImovelID, cascadeDelete: true)
                .Index(t => t.LocalizacaoID)
                .Index(t => t.TipoImovelID)
                .Index(t => t.FotoID);
            
            CreateTable(
                "dbo.Fotoes",
                c => new
                    {
                        FotoID = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.FotoID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Imovels", "TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.Imovels", "LocalizacaoID", "dbo.Localizacaos");
            DropForeignKey("dbo.Imovels", "FotoID", "dbo.Fotoes");
            DropForeignKey("dbo.Anuncios", "ImovelID", "dbo.Imovels");
            DropForeignKey("dbo.Alertas", "TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.Alertas", "LocalizacaoID", "dbo.Localizacaos");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Imovels", new[] { "FotoID" });
            DropIndex("dbo.Imovels", new[] { "TipoImovelID" });
            DropIndex("dbo.Imovels", new[] { "LocalizacaoID" });
            DropIndex("dbo.Anuncios", new[] { "ImovelID" });
            DropIndex("dbo.Alertas", new[] { "TipoImovelID" });
            DropIndex("dbo.Alertas", new[] { "LocalizacaoID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Fotoes");
            DropTable("dbo.Imovels");
            DropTable("dbo.Anuncios");
            DropTable("dbo.TipoImovels");
            DropTable("dbo.Localizacaos");
            DropTable("dbo.Alertas");
        }
    }
}
