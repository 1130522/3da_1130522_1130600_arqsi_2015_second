﻿namespace LocalAccountsApp.Migrations
{
    using Library.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Library.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Library.Models.ApplicationDbContext";
        }

        protected override void Seed(Library.Models.ApplicationDbContext context)
        {
            //Criaçaoo dos roles
                         if (!context.Roles.Any(r => r.Name == "Administrador"))
                             {
                 var store = new RoleStore<IdentityRole>(context);
                 var manager = new RoleManager<IdentityRole>(store);
                 var role = new IdentityRole { Name = "Administrador" };
                 
                 manager.Create(role);
                             }
             
                         if (!context.Roles.Any(r => r.Name == "Cliente"))
                             {
                 var store = new RoleStore<IdentityRole>(context);
                 var manager = new RoleManager<IdentityRole>(store);
                 var role = new IdentityRole { Name = "Cliente" };
                 
                 manager.Create(role);
                             }
             
                         if (!context.Users.Any(u => u.UserName == "admin@gmail.com"))
                             {
                 var store = new UserStore<ApplicationUser>(context);
                 var manager = new UserManager<ApplicationUser>(store);
                 var user = new ApplicationUser { UserName = "admin@gmail.com", Email = "admin@gmail.com" };
                 
                 manager.Create(user, "Abc123!");
                 manager.AddToRole(user.Id, "Administrador");
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Porto"))
                             {
                 var local = new Localizacao { Nome = "Porto" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Aveiro"))
                             {
                 var local = new Localizacao { Nome = "Aveiro" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Lisboa"))
                             {
                 var local = new Localizacao { Nome = "Lisboa" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Gaia"))
                             {
                 var local = new Localizacao { Nome = "Gaia" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Faro"))
                             {
                 var local = new Localizacao { Nome = "Faro" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Alentejo"))
                             {
                 var local = new Localizacao { Nome = "Alentejo" };
                 context.Localizacoes.Add(local);
                             }
             
                         if (!context.Localizacoes.Any(l => l.Nome == "Coimbra"))
                             {
                 var local = new Localizacao { Nome = "Coimbra" };
                 context.Localizacoes.Add(local);
                             }
        }
    }
}
