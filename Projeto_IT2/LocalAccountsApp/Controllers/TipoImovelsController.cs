﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Library.Models;
using Library.DAL.Repository;
using Library.DAL.Interface;
using LocalAccountsApp.DTO;

namespace LocalAccountsApp.Controllers
{
    public class TipoImovelsController : ApiController
    {
        private ITipoImoveisRepository repo = new TipoImoveisRepository();

        // GET: api/TipoImovels
        public IEnumerable<TipoImovelDTO> GetTipoImovel()
        {
            var tipos = from b in repo.GetData()
                        select new TipoImovelDTO()
                        {
                            TipoImovelID = b.TipoImovelID,
                            Nome = b.Nome
                        };

            return tipos;
        }

        // GET: api/TipoImovels/5
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> GetTipoImovel(int id)
        {
            TipoImovel tipo = await repo.GetData(id);
            TipoImovelDTO dto = new TipoImovelDTO()
            {
                TipoImovelID = tipo.TipoImovelID,
                Nome = tipo.Nome
            };

            if (tipo == null)
            {
                return NotFound();
            }

            return Ok(dto);
        }

        // PUT: api/TipoImovels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoImovel(int id, TipoImovel tipoImovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoImovel.TipoImovelID)
            {
                return BadRequest();
            }

            await repo.Update(id, tipoImovel);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoImovels
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> PostTipoImovel(TipoImovel tipoImovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await repo.Create(tipoImovel);

            return CreatedAtRoute("DefaultApi", new { id = tipoImovel.TipoImovelID }, tipoImovel);
        }

        // DELETE: api/TipoImovels/5
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> DeleteTipoImovel(int id)
        {
            bool tipoImovel = await repo.Delete(id);

            if (tipoImovel == false)
            {
                return NotFound();
            }

            return Ok(tipoImovel);
        }

    }
}