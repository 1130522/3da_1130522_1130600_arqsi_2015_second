﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Library.Models;
using Library.DAL.Interface;
using Library.DAL.Repository;
using LocalAccountsApp.DTO;

namespace LocalAccountsApp.Controllers
{
    public class LocalizacaosController : ApiController
    {
        private ILocalizacoesRepository repo = new LocalizacoesRepository();

        // GET: api/Localizacaos
        public IEnumerable<LocalizacaoDTO> GetLocalizacoes()
        {
            var localizacoes = from l in repo.GetData()
                          select new LocalizacaoDTO()
                          {
                             LocalizacaoID = l.LocalizacaoID,
                             Nome = l.Nome
                          };

            return localizacoes;
        }

        // GET: api/Localizacaos/5
        [ResponseType(typeof(Localizacao))]
        public async Task<IHttpActionResult> GetLocalizacoes(int id)
        {
            Localizacao local = await repo.GetData(id);
            if (local == null)
            {
                return NotFound();
            }

            LocalizacaoDTO localDTO = new LocalizacaoDTO()
            {
                LocalizacaoID = local.LocalizacaoID,
                Nome = local.Nome
            };

            return Ok(localDTO);
        }

    }
}