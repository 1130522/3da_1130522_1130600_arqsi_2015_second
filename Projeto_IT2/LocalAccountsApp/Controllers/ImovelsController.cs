﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Library.Models;
using LocalAccountsApp.Controllers;
using Library.DAL;
using LocalAccountsApp.DTO;
using Library.DAL.Repository;
using Library.DAL.Interface;

namespace LocalAccountsApp.Controllers
{
    public class ImovelsController : ApiController
    {
        private IImoveisRepository repo = new ImoveisRepository();
        private IFotosRepository repoFotos = new FotosRepository();

        // GET: api/Imovels
        public IEnumerable<ImovelDTO> GetImovel()
        {
            var imoveis = from b in repo.GetData()
                        select new ImovelDTO()
                        {
                            ImovelID = b.ImovelID,
                            Nome = b.Nome,
                            Localizacao = b.Localizacao.Nome,
                            Area = b.Area,
                            Preco = b.Preco,
                            Latitude = b.GPS.Latitude,
                            Longitude = b.GPS.Longitude,
                            Url = b.Foto.Url,
                            TipoImovel = b.TipoImovel.Nome,
                            Utilizador = b.Utilizador
                        };

            return imoveis;
        }

        public IEnumerable<ImovelDTO> GetImovel(string username)
        {
            var imoveisByUser = from b in repo.GetImovelByUser(username)
                                select new ImovelDTO() {
                                    ImovelID = b.ImovelID,
                                    Nome = b.Nome,
                                    Localizacao = b.Localizacao.Nome,
                                    Area = b.Area,
                                    Preco = b.Preco,
                                    Latitude = b.GPS.Latitude,
                                    Longitude = b.GPS.Longitude,
                                    Url = b.Foto.Url,
                                    TipoImovel = b.TipoImovel.Nome,
                                    Utilizador = b.Utilizador
                                    };
            return imoveisByUser;

        }

        // GET: api/Imovels/5
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> GetImovel(int id)
        {
            Imovel imovel = await repo.GetData(id);
            if (imovel == null)
            {
                return NotFound();
            }

            ImovelDTO imovelDTO = new ImovelDTO()
            {
                Nome = imovel.Nome,
                Area = imovel.Area,
                Preco = imovel.Preco,
                ImovelID = imovel.ImovelID,
                Localizacao = imovel.Localizacao.Nome,
                TipoImovel = imovel.TipoImovel.Nome,
                Url = imovel.Foto.Url,
                Latitude = imovel.GPS.Latitude,
                Longitude = imovel.GPS.Longitude,
                Utilizador = imovel.Utilizador
            };

            return Ok(imovelDTO);
        }

        //PUT: api/Imovels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutImovel(int id, Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                    return BadRequest(ModelState);
            }

            if (id != imovel.ImovelID)
            {
                return BadRequest();
            }

            await repo.Update(id, imovel);

            return StatusCode(HttpStatusCode.NoContent);

        }

        // POST: api/Imovels
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> PostImovel(Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Foto foto = await repoFotos.Create(imovel.Foto);

            imovel.Foto = foto;
            imovel.FotoID = foto.FotoID;

            await repo.Create(imovel);

            return CreatedAtRoute("DefaultApi", new { id = imovel.ImovelID }, imovel);
        }

        // DELETE: api/Imovels/5
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> DeleteImovel(int id)
        {
            bool imovel = await repo.Delete(id);

            if(imovel == false)
            {
                return NotFound();
            }

            return Ok(imovel);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool ImovelExists(int id)
        //{
        //    return db.Imoveis.Count(e => e.ImovelID == id) > 0;
        //}
    }
}