﻿using Library.DAL.Interface;
using Library.DAL.Repository;
using Library.Models;
using LocalAccountsApp.DTO;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace LocalAccountsApp.Controllers
{
    public class AlertasController : ApiController
    {
        private IAlertasRepository repo = new AlertasRepository();

        // GET: api/Alertas
        public IEnumerable<AlertaDTO> GetAlertas()
        {
            var alertas = from a in repo.GetData()
                          select new AlertaDTO()
                          {
                              AlertaID = a.AlertaID,
                              Nome = a.Nome,
                              LocalizacaoNome = a.Localizacao.Nome,
                              PrecoMin = a.PrecoMin,
                              PrecoMax = a.PrecoMax,
                              TipoImovelNome = a.TipoImovel.Nome,
                              TipoAnuncioNome = a.TipoAnuncio.ToString(),
                              Utilizador = a.Utilizador
                          };

            return alertas;
        }

        // GET: api/Alertas/5
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> GetAlerta(int id)
        {
            Alerta alerta = await repo.GetData(id);
            if (alerta == null)
            {
                return NotFound();
            }


            AlertaDTO alertaDTO = new AlertaDTO()
            {
                AlertaID = alerta.AlertaID,
                Nome = alerta.Nome,
                LocalizacaoNome = alerta.Localizacao.Nome,
                PrecoMin = alerta.PrecoMin,
                PrecoMax = alerta.PrecoMax,
                TipoImovelNome = alerta.TipoImovel.Nome,
                TipoAnuncioNome = alerta.TipoAnuncio.ToString(),
                Utilizador = alerta.Utilizador
            };

            return Ok(alertaDTO);
        }

        [ResponseType(typeof(Alerta))]
        public IEnumerable<AlertaDTO> GetAlerta(string username)
        {
            var alertas =from a in repo.GetDataByUser(username)
                         select new AlertaDTO()
                         {
                             AlertaID = a.AlertaID,
                             Nome = a.Nome,
                             LocalizacaoNome = a.Localizacao.Nome,
                             PrecoMin = a.PrecoMin,
                             PrecoMax = a.PrecoMax,
                             TipoImovelNome = a.TipoImovel.Nome,
                             TipoAnuncioNome = a.TipoAnuncio.ToString(),
                             Utilizador = a.Utilizador
                         };
            return alertas;
        }




        //// PUT: api/Alertas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAlerta(int id, Alerta alerta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alerta.AlertaID)
            {
                return BadRequest();
            }

            await repo.Update(id, alerta);   

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Alertas
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> PostAlerta(Alerta alerta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await repo.Create(alerta);

            return CreatedAtRoute("DefaultApi", new { id = alerta.AlertaID }, alerta);
        }

        // DELETE: api/Alertas/5
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> DeleteAlerta(int id)
        {
            bool alerta = await repo.Delete(id);

            if (alerta == false)
            {
                return NotFound();
            }

            return Ok(alerta);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool AlertaExists(int id)
        //{
        //    return db.Alertas.Count(e => e.AlertaID == id) > 0;
        //}
    }
}