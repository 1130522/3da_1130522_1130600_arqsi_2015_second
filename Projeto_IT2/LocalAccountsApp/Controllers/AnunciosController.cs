﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Library.Models;
using Library.DAL.Interface;
using LocalAccountsApp.DTO;
using Library.DAL.Repository;

namespace LocalAccountsApp.Controllers
{
    public class AnunciosController : ApiController
    {
        private IAnunciosRepository repo = new AnunciosRepository();
        private IImoveisRepository repoImovels = new ImoveisRepository();
        // GET: api/Anuncios
        public IEnumerable<AnuncioDTO> GetAnuncios()
        {
            var anuncios = from a in repo.GetData()
                           select new AnuncioDTO()
                           {
                               AnuncioID = a.AnuncioID,
                               DataCriacao = a.DataCriacao,
                               Descricao = a.Descricao,
                               ImovelNome = a.Imovel.Nome,
                               ImovelID = a.ImovelID,
                               ImovelUtilizador = a.Imovel.Utilizador,
                               Nome = a.Nome,
                               LocalizacaoImovel = a.Imovel.Localizacao.Nome,
                               PrecoImovel = a.Imovel.Preco,
                               tipoAnuncio = a.TipoAnuncio.ToString(),
                               TipoImovel = a.Imovel.TipoImovel.Nome,
                               ImovelUrl = a.Imovel.Foto.Url
                               

                           };
            return anuncios;
        }

        // GET: api/Anuncios/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> GetAnuncio(int id)
        {
            Anuncio anuncio = await repo.GetData(id);
            if (anuncio == null)
            {
                return NotFound();
            }

            AnuncioDTO anuncioDTO = new AnuncioDTO()
            {
                AnuncioID = anuncio.AnuncioID,
                DataCriacao = anuncio.DataCriacao,
                Descricao = anuncio.Descricao,
                DescricaoImovel = anuncio.Imovel.Nome,
                ImovelID = anuncio.ImovelID,
                Nome = anuncio.Nome,
                LocalizacaoImovel = anuncio.Imovel.Localizacao.Nome,
                PrecoImovel = anuncio.Imovel.Preco,
                tipoAnuncio = anuncio.TipoAnuncio.ToString(),
                TipoImovel = anuncio.Imovel.TipoImovel.Nome,
                ImovelUrl = anuncio.Imovel.Foto.Url,
                ImovelUtilizador = anuncio.Imovel.Utilizador,
                ImovelNome = anuncio.Imovel.Nome

            };

            return Ok(anuncioDTO);
        }

        //PUT: api/Anuncios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAnuncio(int id, Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != anuncio.AnuncioID)
            {
                return BadRequest();
            }

            await repo.Update(id, anuncio);


            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anuncios
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> PostAnuncio(Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await repo.Create(anuncio);

            return CreatedAtRoute("DefaultApi", new { id = anuncio.AnuncioID }, anuncio);
        }


        // DELETE: api/Anuncios/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> DeleteAnuncio(int id)
        {
            bool anuncio = await repo.Delete(id);

            if (anuncio == false)
            {
                return NotFound();
            }

            return Ok(anuncio);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool AnuncioExists(int id)
        //{
        //    return db.Anuncios.Count(e => e.AnuncioID == id) > 0;
        //}
    }
}