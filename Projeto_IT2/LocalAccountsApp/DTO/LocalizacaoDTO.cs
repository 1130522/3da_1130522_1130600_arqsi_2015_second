﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.DTO
{
    public class LocalizacaoDTO
    {
        public int LocalizacaoID { get; set; }
        public string Nome { get; set; }

        public LocalizacaoDTO() { }

    }
}