﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.DTO
{
    public class AnuncioDTO
    {
        public int AnuncioID { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCriacao { get; set; }
        public string tipoAnuncio { get; set; }
        public int ImovelID { get; set; }
        public string TipoImovel { get; set; }
        public string DescricaoImovel { get; set; }
        public string LocalizacaoImovel { get; set; }
        public float PrecoImovel { get; set; }
        public string ImovelUtilizador { get; set; }
        public string ImovelNome { get; set; }
        public string ImovelUrl { get; set; }

        public AnuncioDTO()
        {

        }
    }
}