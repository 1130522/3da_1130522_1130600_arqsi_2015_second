﻿using Library.Models;

namespace LocalAccountsApp.DTO
{
    public class ImovelDTO
    {
        public int ImovelID { get; set; }
        public string Nome { get; set; }
        public float Area { get; set; }
        public float Preco { get; set; }
        public string Localizacao { get; set; }
        public string TipoImovel { get; set; }
        public string Url { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Utilizador { get; set; }

        public ImovelDTO() {}

    }
}