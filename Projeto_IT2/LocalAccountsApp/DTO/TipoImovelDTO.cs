﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.DTO
{
    public class TipoImovelDTO
    {
        public int TipoImovelID { get; set; }
        public string Nome { get; set; }
    }
}