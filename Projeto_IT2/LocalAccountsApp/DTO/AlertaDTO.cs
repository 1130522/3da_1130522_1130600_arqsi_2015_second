﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocalAccountsApp.DTO
{
    public class AlertaDTO
    {
        public int AlertaID { get; set; }
        public string Nome { get; set; }
        public float PrecoMin { get; set; }
        public float PrecoMax { get; set; }
        public string Utilizador { get; set; }
        public string LocalizacaoNome { get; set; }
        public string TipoImovelNome { get; set; }
        public string TipoAnuncioNome { get; set; }

        public AlertaDTO()
        {
        }
    }
}